﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Monopoly.Model
{
    public partial class Street : UserControl
    {
        private int id;
        private int type;
        private int buyprice;
        private String streetstring = "...";
        private Street next;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public Street Next
        {
            get { return next; }
            set { next = value; }
        }

        public String Streetname
        {
            get { return streetstring; }
            set { streetstring = value; }
        }

        public int Buyprice
        {
            get { return buyprice; }
            set { buyprice = value; }
        }
        public Street()
        {
            InitializeComponent();
        }

        public void setType(int _type)
        {
                this.type = _type;
                updateView();
        }

        public void updateView()
        {
            if (type == 0)
            {
                this.streetname.Text = "";
                this.streettype.Text = "Start";
                this.price.Text = "";
            }

            if (type == 1)
            {
                this.streetname.Text = Streetname;
                this.streettype.Text = "Strasse";
                this.price.Text = buyprice + "€";
            }

            if (type == 2)
            {
                this.streetname.Text = "";
                this.streettype.Text = "Gemeinschaft";
                this.price.Text = "";
            }

            if (type == 3)
            {
                this.streetname.Text = "";
                this.streettype.Text = "Ereignis";
                this.price.Text = "";
            }

            if (type == 4)
            {
                this.streetname.Text = "";
                this.streettype.Text = "Gefängnis";
                this.price.Text = "";
            }

            if (type == 5)
            {
                this.streetname.Text = "";
                this.streettype.Text = "Gehe ins Gefängnis";
                this.price.Text = "";
            }


            if (type == 6)
            {
                this.streetname.Text = "";
                this.streettype.Text = "Frei Parken";
                this.price.Text = "";
            }

            if (type == 7)
            {
                this.streetname.Text = "";
                this.streettype.Text = "Einkommenssteuer";
                this.price.Text = "4000€";
            }

            if (type == 8)
            {
                this.streetname.Text = "";
                this.streettype.Text = "Zusatzsteuer";
                this.price.Text = "2000€";
            }
        }
    }
}
