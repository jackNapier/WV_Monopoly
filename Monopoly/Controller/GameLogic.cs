﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Monopoly.Model;
using Monopoly.Controller;
using System.Drawing;

namespace Monopoly.Controller
{
    static class GameLogic
    {
        public static Boolean started = false;
        private static Gameboard board;
        private static int playerOffset = 5;

        public static void startGame(Gameboard _board)
        {
            
            if (!started)
            {
                board = _board;
                foreach(Player p in board.getPlayers())
                {
                    p.Location = board.getStreets()[0].Location;
                    p.Left += 56 - playerOffset;
                    p.Top += 56;
                    p.Street = board.getStreets()[0];
                    p.Refresh();
                    playerOffset += 5;
                }
                started = true;
                PrologThread pt = new PrologThread();
                pt.testProlog();
            }
        }

        public static void nextPlayer()
        {
            GlobalVariables.activeplayer = ++GlobalVariables.activeplayer;
            if (GlobalVariables.activeplayer >= GlobalVariables.maxplayer) { GlobalVariables.activeplayer = 0; }
        }
        public static void dice()
        {
            Random random = new Random();
            int dice1 = random.Next(1, 7);
            int dice2 = random.Next(1, 7);
            Player currentPlayer = board.getPlayers()[GlobalVariables.activeplayer];
            Street newStreet = currentPlayer.Street;

            for (int i = 0; i <dice1+dice2; i++)
            {
                newStreet = newStreet.Next;
            }
            if (newStreet.Id > 30 && newStreet.Id <= 39)
            {
                currentPlayer.Location = newStreet.Location;
                currentPlayer.Left += 56-playerOffset;
                currentPlayer.Top += 36;
                currentPlayer.Street = newStreet;
                board.setDice1Text(dice1.ToString());
                board.setDice2Text(dice2.ToString());
                board.setStreetText(currentPlayer.Street.Name.ToString());
                nextPlayer();
            }
            else if (newStreet.Id == 30)
            {
                currentPlayer.Location = newStreet.Location;
                currentPlayer.Left += 56;
                currentPlayer.Top += 56;
                currentPlayer.Street = newStreet;
                board.setDice1Text(dice1.ToString());
                board.setDice2Text(dice2.ToString());
                board.setStreetText(currentPlayer.Street.Name.ToString());
                nextPlayer();
            }
            else if (newStreet.Id > 20 && newStreet.Id <= 29)
            {
                currentPlayer.Location = newStreet.Location;
                currentPlayer.Left += 36;
                currentPlayer.Top += 56;
                currentPlayer.Street = newStreet;
                board.setDice1Text(dice1.ToString());
                board.setDice2Text(dice2.ToString());
                board.setStreetText(currentPlayer.Street.Name.ToString());
                nextPlayer();
            }
            else if (newStreet.Id == 20)
            {
                currentPlayer.Location = newStreet.Location;
                currentPlayer.Street = newStreet;
                currentPlayer.Top += 56;
                currentPlayer.Left += 56;
                board.setDice1Text(dice1.ToString());
                board.setDice2Text(dice2.ToString());
                board.setStreetText(currentPlayer.Street.Name.ToString());
                nextPlayer();
            }
            else if (newStreet.Id > 10 && newStreet.Id <= 19)
            {
                currentPlayer.Location = newStreet.Location;
                currentPlayer.Top += 36;
                currentPlayer.Left += 56;
                currentPlayer.Street = newStreet;
                board.setDice1Text(dice1.ToString());
                board.setDice2Text(dice2.ToString());
                board.setStreetText(currentPlayer.Street.Name.ToString());
                nextPlayer();
            }
            else if (newStreet.Id == 10 )
            {
                currentPlayer.Location = newStreet.Location;
                currentPlayer.Top += 56;
                currentPlayer.Left += 36;
                currentPlayer.Street = newStreet;
                board.setDice1Text(dice1.ToString());
                board.setDice2Text(dice2.ToString());
                board.setStreetText(currentPlayer.Street.Name.ToString());
                nextPlayer();
            }

            else if (newStreet.Id > 0 && newStreet.Id <= 9)
            {
                currentPlayer.Location = newStreet.Location;
                currentPlayer.Top += 56;
                currentPlayer.Left += 36;
                currentPlayer.Street = newStreet;
                board.setDice1Text(dice1.ToString());
                board.setDice2Text(dice2.ToString());
                board.setStreetText(currentPlayer.Street.Name.ToString());
                nextPlayer();
            }
            else if (newStreet.Id == 0)
            {
                currentPlayer.Location = newStreet.Location;
                currentPlayer.Top += 56;
                currentPlayer.Left += 56;
                currentPlayer.Street = newStreet;
                board.setDice1Text(dice1.ToString());
                board.setDice2Text(dice2.ToString());
                board.setStreetText(currentPlayer.Street.Name.ToString());
                nextPlayer();
            }
        }
    }
}
